<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Auth\Access\AuthorizationException;

// use Carbon\Carbon;
use App\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', function () {
    return view('welcome');
});


Route::get('/post', function(){
    return \Redirect::to('home');
});
Route::get('/home', 'HomeController@index');
Route::get('/blog', 'PostsController@index')->name('blog.page');
Route::get('/blogs/category/', 'CategoriesController@index');
Route::get('/blogs/category/{id}', array('as' => 'CategoriesController.show', 'uses' => 'CategoriesController@show'));
Route::get('/blogs/{id}', 'PostsController@show');
Route::post('/post', 'PostsController@store')->name('posts.new');
Route::get('/post/trash/{id}', array('as' => 'PostsController.softDestroy', 'uses' => 'PostsController@softDestroy'));
Route::get('/post/destroy/{id}', array('as' => 'PostsController.destroy', 'uses' => 'PostsController@destroy'));
Route::get('/post/restore/{id}', array('as' => 'PostsController.restore', 'uses' => 'PostsController@restore'));
Route::post('/comment', 'CommentsController@store')->name('comment.new');
Route::post('/comment/reply', 'CommentsController@reply')->name('comment.reply');
Route::get('/comment/like/{id}', 'CommentsController@commentLike');


// Route::get('admin/home/test',array('as'=>'admin.home',function(){
//     $url = route('admin.home');
//     return "This is test". $url;
// }));

// middleware
Route::group(['middleware' => ['web','role']], function(){
    Route::resource('/posts', 'PostsController');
    // Route::get('/dates',function(){
    //     echo Carbon::now()->subMonths(5)->yesterday()->diffForHumans();
    // });
});

