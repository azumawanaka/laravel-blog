@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card-header">Blogs</div>
    <div class="card-body">
        <div class="row">
            @foreach ($blogs as $post)
            <div class="col-md-4 mb-4">
                <div class="card h-100">
                    <div class="card-body">
                        <div class="img-thumbnail card-img mb-2">
                            @if (!empty($post->img))
                               <img src="{{URL::asset('uploads')."/".$post->img}}" alt="">
                            @else
                                <img src="https://placehold.it/1000x1000" id="preview" class="img-fluid">
                            @endif
                        </div>
                        {!! empty($post->title) ? "<h4 class='text-muted text-center card-title mb-0'>No Title</h4>" : "<h4 class='card-title text-center text-primary mb-0'>".ucfirst($post->title)."</h4>"; !!}</h2>
                        
                        <?php
                            $cat = $post->cat;
                            $cat_split = explode(",", $cat);
                            foreach ($cat_split as $val) {
                                echo '<small><a href="'.url('/blogs/category').'/'.$val.'" class="badge badge-secondary mr-1">'.$val.'</a></small>';
                            }    
                        ?>
                        <small class="text-muted d-block">Author: {{$post->name}}</small>
                        <small class="text-mmuted">Date Posted: {{$post->updated_at}}</small>
                        <div class="mt-3">
                            {{mb_strimwidth($post->desc, 0,70, ".....")}}
                        </div>
                    </div>
                    <div class="card-footer">
                        <a href="{{url('blogs')}}/{{$post->title}}" class="btn btn-block btn-primary">
                            View Post
                        </a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        {{-- paginate --}}
        <div class="d-block p-3">
            <?php echo $blogs->render(); ?>
        </div>
        {{-- //paginate --}}
    </div>
</div>

@endsection
