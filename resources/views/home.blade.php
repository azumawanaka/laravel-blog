@extends('layouts.app')

@section('content')
<div class="container">
    @if ($errors->any())
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            @foreach ($errors->all() as $error)
                <div><i class="fa fa-arrow-right"></i> {!! $error !!}</div>
            @endforeach
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="card-header">Add Post</div>
            <div class="card-body">
                <div id="msg"></div>
                <form action="{{route('posts.new')}}" method="post" id="add_post" accept-charset="utf-8" enctype="multipart/form-data">
                    
                    @csrf
                    <div class="form-group">
                        <div class="form-group">
                            <input type="text" class="form-control" name="title" placeholder="Enter title..">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-group">
                                <textarea name="desc" id="desc" cols="30" rows="10" class="form-control" placeholder="Place any text here.."></textarea>
                            </div>
                            <div class="form-group">
                                <label for="category" class="col-md-12 pl-0">Category :</label>
                                <input type="text" id="category" value="PHP,WP,HTML5,CSS3,JS" name="category" data-role="tagsinput" >
                            </div>
                            <input type="submit" class="btn btn-primary btn-block" value="Save">
                        </div>
                        <div class="col-md-3">
                            <div class="ml-0 pl-0">
                                <img src="https://placehold.it/250x250" id="preview" class="img-thumbnail">
                            </div>
                            <input type="file" name="img" class="file" accept="image/*">
                            <div class="input-group my-3">
                                <input type="text" class="form-control" disabled placeholder="Upload File" id="file">
                                <div class="input-group-append">
                                    <button type="button" class="browse btn btn-primary">Browse...</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            
            <div class="card-body">
                <h3>Active Posts</h3>
                <div class="table-responsive">
                    <table class="table table-hover dataTable">
                        <thead class="table-info">
                            <tr>
                                <th>ID</th>
                                <th width="200">Title</th>
                                <th width="200">Content</th>
                                <th width="80">Img</th>
                                <th width="200">Categories</th>
                                <th width="120">Date Created</th>
                                <th width="120">Date Updated</th>
                                <th>Date Deleted</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($posts as $post)
                                <tr>
                                    <td>{{$post->id}}</td>
                                    <td><a href="{{url('blogs')}}/{{$post->title}}">{!! empty($post->title) ? "No Title" : $post->title !!}</a></td>
                                    <td>
                                        <?php
                                        // The Regular Expression filter
                                        $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
        
                                        // The Text you want to filter for urls
                                        $text = $post->desc;
        
                                        // Check if there is a url in the text
                                        if(preg_match($reg_exUrl, $text, $url)) {
        
                                            // make the urls hyper links
                                            echo nl2br(preg_replace($reg_exUrl, "<a href=".$url[0]." target='_blank'>".$url[0]."</a> ", $text));
        
                                        } else {
        
                                            // if no urls in the text just return the text
                                            echo nl2br($text);
        
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        @if (!empty($post->img))
                                            <img src="{{URL::asset('uploads')."/".$post->img}}" alt="" class="img-thumbnail">
                                        @endif
                                    </td>
                                    <td>
                                        <?php
                                            $categ = $post->cat;
                                            $cat_split = explode(",", $categ);
                                            $class = 'badge-primary';
                                            foreach ($cat_split as $val) {
                                                echo '<a href="'.url('/blogs/category').'/'.$val.'" class="badge '.$class.' mr-1">'.$val.'</a>';
                                            }      
                                        ?>
                                    </td>
                                    <td>{{$post->created_at}}</td>
                                    <td>{{$post->updated_at}}</td>
                                    <td>{{$post->deleted_at}}</td>
                                    <td>
                                        <a href="{{ url("/") }}/post/trash/{{$post->id}}" class="btn btn-border-primary text-danger">
                                            <i class="fa fa-trash"></i> Trash
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <h3>Trashed Posts</h3>
                <div class="table-responsive">
                    <table class="table table-hover dataTable">
                        <thead class="table-info">
                            <tr>
                                <th>ID</th>
                                <th width="200">Title</th>
                                <th width="200">Content</th>
                                <th width="80">Img</th>
                                <th width="200">Categories</th>
                                <th>Date Deleted</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($trashed_posts as $post)
                                <tr>
                                    <td>{{$post->id}}</td>
                                    <td><a href="{{url('blogs')}}/{{$post->title}}">{!! empty($post->title) ? "No Title" : $post->title !!}</a></td>
                                    <td>
                                        <?php
                                        // The Regular Expression filter
                                        $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
        
                                        // The Text you want to filter for urls
                                        $text = $post->desc;
        
                                        // Check if there is a url in the text
                                        if(preg_match($reg_exUrl, $text, $url)) {
        
                                            // make the urls hyper links
                                            echo nl2br(preg_replace($reg_exUrl, "<a href=".$url[0]." target='_blank'>".$url[0]."</a> ", $text));
        
                                        } else {
        
                                            // if no urls in the text just return the text
                                            echo nl2br($text);
        
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        @if (!empty($post->img))
                                            <img src="{{URL::asset('uploads')."/".$post->img}}" alt="" class="img-thumbnail">
                                        @endif
                                    </td>
                                    <td>
                                        <?php
                                            $categ = $post->cat;
                                            $cat_split = explode(",", $categ);
                                            $class = 'badge-primary';
                                            foreach ($cat_split as $val) {
                                                echo '<a href="'.url('/blogs/category').'/'.$val.'" class="badge '.$class.' mr-1">'.$val.'</a>';
                                            }      
                                        ?>
                                    </td>
                                    <td>{{$post->deleted_at}}</td>
                                    <td>
                                        <a href="{{ url("/") }}/post/restore/{{$post->id}}" class="text-info mr-2">
                                            <i class="fa fa-refresh"></i> Retrieve
                                        </a>
                                        <a href="{{ url("/") }}/post/destroy/{{$post->id}}" class="text-danger">
                                            <i class="fa fa-remove"></i> Delete 
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{-- @foreach ($trashed_posts as $trashed)
                    <div class="card mb-4">
                        <div class="mb-4 card-header d-flex justify-content-between">
                            {!! empty($trashed->title) ? "<h3 class='text-muted'>No Title</h3>" : "<h3>".$trashed->title."</h3>"; !!}</h2>
                            
                            <div>
                                <a href="{{ url("/") }}/post/restore/{{$trashed->id}}" class="text-info mr-2">
                                    <i class="fa fa-refresh"></i> retrieve
                                </a>
                                <a href="{{ url("/") }}/post/destroy/{{$trashed->id}}" class="text-danger">
                                    <i class="fa fa-remove"></i> Delete 
                                </a>
                            </div>
                        </div>
                        <div class="p-3 pt-0">
                            <?php
                                $cat = $trashed->name;
                                $cat_split = explode(",", $cat);
                                foreach ($cat_split as $val) {
                                    echo '<a href="#" class="badge badge-primary mr-1">'.$val.'</a>';
                                }    
                            ?>
                        </div>
                        <div class="mb-4 p-3">
                            <?php
                                // The Regular Expression filter
                                $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";

                                // The Text you want to filter for urls
                                $text = $trashed->desc;

                                // Check if there is a url in the text
                                if(preg_match($reg_exUrl, $text, $url)) {

                                    // make the urls hyper links
                                    echo nl2br(preg_replace($reg_exUrl, "<a href=".$url[0]." target='_blank'>".$url[0]."</a> ", $text));

                                } else {

                                    // if no urls in the text just return the text
                                    echo nl2br($text);

                                }
                                ?>
                        </div>
                        @if (!empty($trashed->img))
                            <div class="col-md-5 p-3"><img src="{{URL::asset('uploads')."/".$trashed->img}}" alt="" class="img-fluid"></div>
                        @endif
                        <div class="text-right mt-2 p-3 text-muted">
                            {{$trashed->updated_at}}
                        </div>
                    </div>
                @endforeach --}}
            </div>
        </div>
    </div>
</div>

@endsection

@section('footer')
{{-- file upload+preview --}}
<script>
    $(document).on("click", ".browse", function() {
        var file = $(this).parents().find(".file");
        file.trigger("click");
    });
    $('input[type="file"]').change(function(e) {
        var fileName = e.target.files[0].name;
        $("#file").val(fileName);

        var reader = new FileReader();
        reader.onload = function(e) {
            // get loaded data and render thumbnail.
            document.getElementById("preview").src = e.target.result;
        };
        // read the image file as a data URL.
        reader.readAsDataURL(this.files[0]);
    });
</script>
@endsection