@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card-body">
                <h4 class="card-header mb-3">
                    {{ucfirst($post->title)}}
                </h4>
                @if (!empty($post->img))
                    <div class="text-center">
                        <img src="{{URL::asset('uploads')."/".$post->img}}" alt="" class="mb-4 img-thumbnail" style="height: 400px;">
                    </div>
                @endif
                <div class="pl-3 pr-3">
                <?php
                        // The Regular Expression filter
                        $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";

                        // The Text you want to filter for urls
                        $text = $post->desc;

                        // Check if there is a url in the text
                        if(preg_match($reg_exUrl, $text, $url)) {

                            // make the urls hyper links
                            echo nl2br(preg_replace($reg_exUrl, "<a href=".$url[0]." target='_blank'>".$url[0]."</a> ", $text));

                        } else {

                            // if no urls in the text just return the text
                            echo nl2br($text);

                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <h4 class="mt-5 text-muted">Comments ({{count($comments)}})</h4>
    <div class="row mb-5">
        @foreach ($comments as $com)
            <div class="col-md-12 mb-3">
                <div class="media g-mb-30 media-comment">
                    <img class="d-flex g-width-50 g-height-50 rounded-circle g-mt-3 g-mr-15" src="https://bootdey.com/img/Content/avatar/avatar{{rand(1,8)}}.png" alt="Image Description">
                    <div class="media-body u-shadow-v18 g-bg-secondary pb-3">
                        <div class="g-mb-15">
                            <h5 class="h5 g-color-gray-dark-v1 mb-0">{{$com->fullname}}</h5>
                            <span class="g-color-gray-dark-v4 g-font-size-12">{{$com->updated_at->diffForHumans()}}</span>
                        </div>
                    
                        <p class="pl-3">
                            {!!$com->comments!!}
                        </p>
                    
                        <ul class="list-inline d-sm-flex my-0">
                            <li class="list-inline-item g-mr-20">
                                <a class="u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="{{url('comment/like')}}/{{$com->id}}">
                                    <i class="fa fa-thumbs-up g-pos-rel g-top-1 g-mr-3"></i>
                                    178
                                </a>
                            </li>
                            <li class="list-inline-item g-mr-20">
                                <a class="u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="{{url('comment/like')}}/{{$com->id}}">
                                    <i class="fa fa-thumbs-down g-pos-rel g-top-1 g-mr-3"></i>
                                    34
                                </a>
                            </li>
                            <li class="list-inline-item g-mr-20">
                                <span class="u-link-v5 g-color-gray-dark-v4 g-color-primary--hover">
                                    <i class="fa fa-comments g-pos-rel g-top-1 g-mr-3"></i>
                                    {{count($com->replies)}}
                                </span>
                            </li>
                            <li class="list-inline-item ml-auto">
                                <a class="u-link-v5 g-color-gray-dark-v4 g-color-primary--hover comment_reply" href="#" data-attr="reply_{{$com->id}}">
                                    <i class="fa fa-reply g-pos-rel g-top-1 g-mr-3"></i>
                                    Reply
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                {{-- replies --}}
                @foreach ($com->replies as $rep) 
                    <div class="ml-5 pl-3 mb-1">
                        <div class="media g-mb-30 media-comment">
                            <img class="d-flex g-width-50 g-height-50 rounded-circle g-mt-3 g-mr-15" src="https://bootdey.com/img/Content/avatar/avatar{{rand(1,8)}}.png" alt="Image Description">
                            <div class="media-body u-shadow-v18 g-bg-secondary pb-3">
                                <div class="g-mb-15">
                                    <h5 class="h6 g-color-gray-dark-v1 mb-0 text-muted">{{$rep->name}}</h5>
                                    <span class="g-color-gray-dark-v4 g-font-size-12">
                                        {{$rep->updated_at->diffForHumans()}}
                                    </span>
                                </div>
                            
                                <p class="pl-3">
                                    {!!nl2br($rep->content)!!}
                                </p>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="row mt-4 ml-5 reply_section" id="reply_{{$com->id}}">
                    <div class="col-md-12">
                        <form action="{{route('comment.reply')}}" class="form-horizontal" method="POST">
                            @csrf
                            <input type="hidden" name="comment_id" id="comment_id_{{$com->id}}" value="{{$com->id}}">
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" id="email_{{$com->id}}" placeholder="Enter your email address" required>
                            </div>
                            <div class="form-group">
                                <textarea name="reply" name="reply"  class="form-control" id="rep_{{$com->id}}" cols="30" rows="5" placeholder="Reply to {{$com->fullname}}"></textarea>
                            </div>
                            <div class="form-group text-right">
                                <input type="submit" class="btn btn-success btn-md" value="Submit">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <h3>Comment Section</h3>
    @guest
        <div class="row mt-3">
            <div class="col-md-12">
                <form action="{{route('comment.new')}}" class="form-horizontal" method="POST">

                    @csrf
                    <input type="hidden" name="post_id" id="post_id" value="{{$post->id}}">
                    <div class="form-group">
                        <input type="text" class="form-control" name="fullname" id="fullname" minlength="3" placeholder="Enter your name" required>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" name="email" id="email" placeholder="****@sample.com" required>
                    </div>
                    <div class="form-group">
                        <textarea name="comment" name="comment"  class="form-control" id="comment" cols="30" rows="5" placeholder="Enter comments here.."></textarea>
                    </div>
                    <div class="form-group text-right">
                        <input type="submit" class="btn btn-primary btn-md" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    @else

        @if ( auth()->user()->id != $post->user_id )
            <div class="row mt-3">
                <div class="col-md-12">
                    <form action="{{route('comment.new')}}" class="form-horizontal" method="POST">

                        @csrf
                        <input type="hidden" name="post_id" id="post_id" value="{{$post->id}}">
                        <div class="form-group">
                            <input type="text" class="form-control" name="fullname" id="fullname" minlength="3" placeholder="Enter your name" required>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" name="email" id="email" placeholder="****@sample.com" required>
                        </div>
                        <div class="form-group">
                            <textarea name="comment" name="comment"  class="form-control" id="comment" cols="30" rows="5" placeholder="Enter comments here.."></textarea>
                        </div>
                        <div class="form-group text-right">
                            <input type="submit" class="btn btn-primary btn-md" value="Submit">
                        </div>
                    </form>
                </div>
            </div>
        @endif
    @endguest
</div>

@endsection