@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card-header">Blog Categories</div>
    <div class="card-body">
        <div class="row">
            @foreach ($categories as $cat)
            <div class="col-md-4 mb-4">
                <div class="card h-100">
                    <div class="card-body">
                        <div class="img-thumbnail card-img mb-2">
                            @if (!empty($cat->img))
                               <img src="{{URL::asset('uploads')."/".$cat->img}}" alt="">
                            @else
                                <img src="https://placehold.it/1000x1000" id="preview" class="img-fluid">
                            @endif
                        </div>
                        {!! empty($cat->title) ? "<h4 class='text-muted text-center card-title mb-0'>No Title</h4>" : "<h4 class='card-title text-center text-primary mb-0'>".ucfirst($cat->title)."</h4>"; !!}</h2>
                        
                        <?php
                            $categ = $cat->categ;
                            $cat_split = explode(",", $categ);
                            $class = 'badge-primary';
                            foreach ($cat_split as $val) {
                                if( $val !== $cn ) {
                                    $class = 'badge-secondary';
                                }else{
                                    $class = 'badge-primary';
                                }
                                echo '<a href="'.url('/blogs/category').'/'.$val.'" class="badge '.$class.' mr-1">'.$val.'</a>';
                            }    
                        ?>
                        <small class="text-muted d-block mt-2">Author:
                            @if ($cat->author == auth()->user()->name)
                                me
                            @else
                                {{$cat->author}}
                            @endif
                        </small>
                        <small class="text-mmuted">Date Posted: {{$cat->updated_at}}</small>
                        <div class="mt-3">
                            {{mb_strimwidth($cat->desc, 0,70, ".....")}}
                        </div>
                    </div>
                    <div class="card-footer">
                        <a href="{{url('blogs')}}/{{$cat->title}}" class="btn btn-block btn-primary">
                            View Post
                        </a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        
        {{-- paginate --}}
        <div class="d-block p-3">
            <?php echo $categories->render(); ?>
        </div>
        {{-- //paginate --}}
    </div>
</div>

@endsection
