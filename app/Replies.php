<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Replies extends Model
{
    protected $fillable = [
        'comments_id', 'name', 'email', 'content'
    ];
}
