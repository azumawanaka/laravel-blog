<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use Redirect;

use App\Comments;
use App\Replies;
use App\Likes;

class CommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $store_data = Comments::create([
            'post_id'   => $request->input('post_id'),
            "fullname"  => $request->input('fullname'),
            "email" => $request->input('email'),
            "comments"  => $request->input('comment')
        ]);

        if($store_data) {
            $msg = array("type" => "success", "title" => "Success!", "msg" => "Thank you for your comment.");
        }else{
            $msg = array("type" => "danger", "title" => "Error!", "msg" => "Something went wrong. Please try again later.");
        }

        return Redirect::back()->with('message', $msg);
    }

    public function reply(Request $request) {
        // check email if exist already inside replies table
        $reply = Replies::where('email', $request->input('email'))
                            ->select('name')
                            ->first();
        if( !empty($reply->name) ) {
            $name = $reply->name;
        }else{
            // generate random user name
            // generate only if new user using email
            $num = substr(str_shuffle(str_repeat("1234567890", 4)), 0, 4);
            $name = "user_".$num;
        }

        // store data

        $store_rep = Replies::create([
            "comments_id"    => $request->input('comment_id'),
            "name"  => $name,
            "email" => $request->input('email'),
            "content"   => $request->input('reply')
        ]);

        // get comment name
        $com_n = Comments::where('id', $request->input('comment_id'))
                            ->select('fullname')
                            ->first();

        if($store_rep) {
            $msg = array("type" => "success", "title" => "Success!", "msg" => "You reply to ".$com_n->fullname."'s thread.");
        }else{
            $msg = array("type" => "danger", "title" => "Error!", "msg" => "Something went wrong. Please try again later.");
        }

        return Redirect::back()->with('message', $msg);
    }
    
    public function commentLike($id) {
        $findRep = Likes::where('comments_id', $id)
                        ->where('users_id', auth()->user()->id)
                        ->select('comments_id','counter')
                        ->first();
        if(empty($findRep->comments_id)) {
            // insert as new data
            $store_like = Likes::create([
                'comments_id'   => $id,
                'users_id'  => auth()->user()->id,
                'counter'   => 1
            ]);
        }else{
            // update
            $store_like = Likes::where('comments_id', $id)
                    ->where('users_id', auth()->user()->id)
                    ->update([
                        'counter'   => $findRep->counter - 1
                    ]);
            if($findRep->counter == 1) {
                // delete record
                $store_like = Likes::where('comments_id', $id)
                                    ->where('users_id', auth()->user()->id)
                                    ->forceDelete();
            }
        }

        if($store_like) {
            $msg = array("type" => "success", "title" => "Success!", "msg" => "You successfully like this thread.");
        }else{
            $msg = array("type" => "danger", "title" => "Error!", "msg" => "Something went wrong. Please try again later.");
        }

        return Redirect::back()->with('message', $msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
