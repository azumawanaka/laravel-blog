<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use Redirect;
use Session;
use DB;
use Auth;
use Response;
use Config;
use App\Posts;
use App\Categories;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query_cat = \DB::table('posts')
                        ->leftJoin('categories', 'posts.id', '=', 'categories.post_id')
                        ->leftJoin('users', 'posts.user_id', '=', 'users.id')
                        ->select('posts.*', 'users.name as author','categories.name as categ')
                        ->orderBy("posts.updated_at", "desc")
                        ->where('categories.name','like','%'.$id.'%')
                        ->whereNull('deleted_at')
                        ->paginate(3);

        if(count($query_cat) < 1) {
            return abort(404);
        }
        $category_n = $id;
        return view("category", ["categories" => $query_cat, "cn" => $category_n]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
