<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Requests\CreatePostsRequest;
use Illuminate\Support\Facades\Mail;

use App\Mail\SendMailable;

use Redirect;
use Session;
use DB;
use Auth;
use Response;
use Config;
use File;

use App\Posts;
use App\Categories;
use App\Comments;
use App\Replies;
use App\Likes;


class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query_posts = \DB::table('posts')
                        ->leftJoin('users', 'posts.user_id', '=', 'users.id')
                        ->leftJoin('categories', 'posts.id', '=', 'categories.post_id')
                        ->select('posts.*', 'users.name','categories.name as cat')
                        ->orderBy("posts.updated_at", "desc")
                        ->whereNull('deleted_at')
                        ->paginate(3);
        return view("blog", ["blogs" => $query_posts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\CreatePostsRequest $request)
    {
        if(!$request->all()) {
            return redirect()->back();
        }else{
            $files = $request->file('img');
            $img = "";
            $uid = auth()->user()->id;
            if (!empty($files)) {
                $destinationPath = 'uploads/'; // upload path
                $img = $uid.'_'.date('mdY') . "." . $files->getClientOriginalExtension();
                $files->move($destinationPath, $img);
            }

            $query = Posts::create([
                'user_id'   => $uid,
                'title' => $request->input('title'),
                'desc' => $request->input('desc'),
                'img'   => $img
            ]);

            Categories::create([
                'post_id'   => $query->id,
                'name' => strtolower($request->input('category'))
            ]);

            if($query) {
                $msg = array("type" => "success", "title" => "Success!", "msg" => "New post was successfully added.");
            }else{
                $msg = array("type" => "danger", "title" => "Error!", "msg" => "Something went wrong. Please try again later.");
            }

            return Redirect::back()->with('message', $msg);
        }
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        // fetch post title
        $query_posts = Posts::where('title', $id)
                            ->first();

        $query_comments = Comments::with('replies')
                            ->where('post_id', $query_posts->id)
                            ->orderBy('comments.updated_at','desc')
                            ->get();


        return view('single-post', [
                                    "post" => $query_posts, 
                                    "comments" => $query_comments
                                    ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $flag = false;
        $uid = auth()->user()->id;
        $img_f = \DB::table('posts')
                        ->where('id',$id)
                        ->select('img')
                        ->first();

        $path = public_path('uploads/'.$img_f->img);
        if(\File::exists($path)){
            \File::delete($path);
        }
        // force delete
        $post = Posts::withTrashed()
                    ->where('id', $id)
                    ->forceDelete();

        if($post) {
            $msg = array("type" => "success", "title" => "Success!", "msg" => "Post was successfully deleted.");
        }else{
            $msg = array("type" => "danger", "title" => "Error!", "msg" => "Something went wrong. Please try again later.");
        }
        
        return \Redirect::to('home')->with('message', $msg);
    }


    public function softDestroy($id) {
        // soft delete
        $del = Posts::find($id)->delete();
        if($del) {
            $msg = array("type" => "success", "title" => "Success!", "msg" => "Post was successfully move to trash.");
        }else{
            $msg = array("type" => "danger", "title" => "Error!", "msg" => "Something went wrong. Please try again later.");
        }
        return \Redirect::to('home')->with('message', $msg);
    }

    public function restore($id) {
        // restore trashed item
        $res = Posts::withTrashed()
                    ->where('id', $id)
                    ->restore();
        if($res) {
            $msg = array("type" => "success", "title" => "Success!", "msg" => "Post was successfully restored.");
        }else{
            $msg = array("type" => "danger", "title" => "Error!", "msg" => "Something went wrong. Please try again later.");
        }
        return \Redirect::to('home')->with('message', $msg);
    }
}
