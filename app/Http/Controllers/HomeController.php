<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;

use Redirect;
use Session;
use DB;
use Auth;
use Response;
use Config;
use App\Posts;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $query_posts = \DB::table('posts')
                        ->leftJoin('categories', 'posts.id', '=', 'categories.post_id')
                        ->select('posts.*','categories.name as cat')
                        ->orderBy("posts.updated_at", "desc")
                        ->where('user_id', auth()->user()->id)
                        ->whereNull('deleted_at')
                        ->get();

        $query_trashed_posts = Posts::onlyTrashed()
                            ->where('user_id',auth()->user()->id)
                            ->orderBy("deleted_at","desc")
                            ->get();
        

        return view("home", ["posts" => $query_posts, "trashed_posts" => $query_trashed_posts]);
    }
}
