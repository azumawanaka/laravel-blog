<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    protected $fillable = [
        'post_id', 'fullname', 'email', 'comments'
    ];

    public function replies() {
        return $this->hasMany('App\Replies');
    }
}
